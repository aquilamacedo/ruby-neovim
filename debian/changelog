ruby-neovim (0.9.1-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Use canonical URL in Vcs-Git.
  * Update standards version to 4.4.1, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update renamed lintian tag names in lintian overrides.
  * Update standards version to 4.5.1, no changes needed.
  * Update lintian override info format in d/ruby-neovim.lintian-overrides on
    line 3.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

  [ Aquila Macedo ]
  * New upstream version 0.9.1
  * d/control: replaced 'ruby-interpreter' to '${ruby:Depends}'.
  * d/patches: update header set Forwarded field to 'not-needed'.

 -- Aquila Macedo Costa <aquilamacedo@riseup.net>  Thu, 07 Sep 2023 11:08:44 +0530

ruby-neovim (0.8.1-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Jason Pleau ]
  * New upstream release
  * Refresh patches
  * add .pc/ to gitignore
  * Point Vcs-* fields to salsa.debian.org
  * Update copyright years
  * Update Standards-Version to 4.4.0, no changes
  * Remove d/compat and build-depend on debhelper-compat

  [ Antonio Terceiro ]
  * debian/watch: update to point to gemwatch.debian.net

 -- Jason Pleau <jason@jpleau.ca>  Sat, 14 Sep 2019 14:53:56 -0400

ruby-neovim (0.7.1-1) unstable; urgency=medium

  * New upstream release
  * Update debhelper compat to 11
  * Removed patches no longer needed:
    - disable_documentation_version_test
    - disable_shada
  * Update Standards-Version to 4.1.3 (no changes were needed)
  * Disable acceptance tests

 -- Jason Pleau <jason@jpleau.ca>  Sat, 03 Mar 2018 18:46:45 -0500

ruby-neovim (0.6.1-1) unstable; urgency=medium

  * New upstream release
  * Refreshed patch remove_coveralls_pry_bundler.patch
  * Removed patches no longer needed:
    - disable_failing_test_schroot.patch
    - fix_current_window_test.patch
    - require_tempfile.patch
  * New patches:
    - disable_documentation_version_test.patch
    - disable_shada.patch
  * Bump Standards-Version to 4.1.1 (no changes were needed)
  * Bump debhelper compatibility to 10
  * Add ruby-multi-json as a "Depends" and "Build-Depends"

 -- Jason Pleau <jason@jpleau.ca>  Fri, 24 Nov 2017 16:25:02 +0100

ruby-neovim (0.3.1-1) unstable; urgency=medium

  * Initial release (Closes: #835379)

 -- Jason Pleau <jason@jpleau.ca>  Fri, 09 Dec 2016 18:15:05 -0500
